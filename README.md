# Smartgears4 Hello World

HelloWorld Service for Smartgears 4

## Structure of the project

* The source code is present in the `src` folder
* The `dockerize` folder contains all the files needed to build and exec the docker image
* The `documentation` folder contains all the documentation and instructions files

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

For details on the execution of the project and the  project structure see  the [Instructions.md](./documentation/Instructions.md) file
For the configuration of JDK and/or maven on MacOS follow the [macos_jdk.md](./documentation/macos_jdk.md) file
For the configuration of Sphinx on MacOS follow the [macos_sphinx.md](./documentation/macos_sphinx.md) file

## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Luca Frosini** ([ORCID](https://orcid.org/0000-0003-3183-2291)) - [ISTI-CNR Infrascience Group](https://infrascience.isti.cnr.it/)* **Alfredo Oliviero** ([ORCID]( https://orcid.org/0009-0007-3191-1025)) - [ISTI-CNR Infrascience Group](https://infrascience.isti.cnr.it/)

## License

This project is licensed under the license **EUPL V. 1.2** - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework

This software is part of the [gCubeFramework](https://www.gcube-system.org/gCubeFramework): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.

The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)