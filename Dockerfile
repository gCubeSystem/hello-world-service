# docker build -t $DOCKER_BUILD_NAME \
#     --build-arg="MVN_FINALNAME=$MVN_FINALNAME" \
#     --build-arg="MVN_NAME=${MVN_NAME}" \
#     --build-arg="CONTAINER_INI=${CONTAINER_INI}" \
#     --build-arg="SMARTGEAR_IMAGE=${SMARTGEAR_IMAGE}" \
#     --build-arg="PORT=${PORT}" \
#     -f Dockerfile \
#     $PLATFORMS .

ARG SMARTGEAR_IMAGE

FROM ${SMARTGEAR_IMAGE}
    ARG CONTAINER_INI
    ARG MVN_FINALNAME
    ARG MVN_NAME
    ARG PORT

    COPY ./dockerize/configuration/logback.xml /etc/
    COPY ./dockerize/configuration/*.gcubekey /tomcat/lib
    
    COPY ./target/${MVN_FINALNAME}.war ${WEBAPPS_DIR}/${MVN_NAME}.war
    
    RUN apt-get update
    # tree is a useful tool to check the directory structure
    RUN apt-get install -y tree
    # In the case you have to copy a file inside the service directory 
    # the war must be unzipped
    # RUN apt-get install -y unzip
    # ENV SERVICE_DIR=${WEBAPPS_DIR}/${MVN_NAME}
    # RUN unzip ${WEBAPPS_DIR}/${MVN_NAME}.war -d ${SERVICE_DIR}
    # COPY ./dockerize/configuration/config.properties ${SERVICE_DIR}/WEB-INF/classes/
   
    COPY ${CONTAINER_INI} /etc/container.ini

    EXPOSE ${PORT}
