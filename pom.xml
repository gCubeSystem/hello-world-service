<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.gcube.tools</groupId>
		<artifactId>maven-parent</artifactId>
		<version>1.2.0</version>
	</parent>

	<groupId>org.gcube.service</groupId>
	<artifactId>hello-world-service</artifactId>
	<version>1.0.0-SNAPSHOT</version>
	<name>Hello World Service</name>
	<!--  DO NOT use tabs or new lines in description otherwise it corrupts application.yaml file -->
	<description>Hello World Service is used as reference project template for Smartgears 4 based services.</description>
	<packaging>war</packaging>

	<organization>
    	<name>gCube System</name>
    	<url>https://www.gcube-system.org/</url>
  	</organization>
	
	<developers>
	    <developer>
			<id>luca.frosini</id>
			<name>Luca Frosini</name>
			<email>luca.frosini@isti.cnr.it</email>
			<url>https://www.isti.cnr.it/en/about/people-detail/141/Frosini_Luca</url>
			<organization>ISTI-CNR</organization>
	      	<organizationUrl>https://www.isti.cnr.it/</organizationUrl>
			<roles>
				<role>researcher</role>
				<role>developer</role>
			</roles>
			<timezone>Europe/Rome</timezone>
	    </developer>
	    <developer>
			<id>lucio.lelii</id>
			<name>Lucio Lelii</name>
			<email>lucio.lelii@isti.cnr.it</email>
			<url></url>
			<organization>ISTI-CNR</organization>
	      	<organizationUrl>https://www.isti.cnr.it/en/about/people-detail/184/Lelii_Lucio/</organizationUrl>
			<roles>
				<role>researcher</role>
				<role>developer</role>
			</roles>
			<timezone>Europe/Rome</timezone>
	    </developer>
	    <developer>
			<id>alfredo.oliviero</id>
			<name>Alfredo Oliviero</name>
			<email>alfredo.oliviero@isti.cnr.it</email>
			<url></url>
			<organization>ISTI-CNR</organization>
	      	<organizationUrl>https://www.isti.cnr.it/en/about/people-detail/1087/Oliviero_Alfredo/</organizationUrl>
			<roles>
				<role>researcher</role>
				<role>developer</role>
			</roles>
			<timezone>Europe/Rome</timezone>
	    </developer>
	</developers>

	<properties>
		<java.version>17</java.version>
		<maven.compiler.source>${java.version}</maven.compiler.source>
		<maven.compiler.target>${java.version}</maven.compiler.target>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<!-- OPTIONAL. for authorization-control-library -->
		<!-- <aspectj-plugin.version>1.14.0</aspectj-plugin.version> -->
		<enunciate.version>2.17.1</enunciate.version>
		<docker.smartgear.version>4.0.0-SNAPSHOT</docker.smartgear.version>
	</properties>

	<scm>
		<connection>scm:git:https://code-repo.d4science.org/gCubeSystem/${project.artifactId}.git</connection>
		<developerConnection>scm:git:https://code-repo.d4science.org/gCubeSystem/${project.artifactId}.git</developerConnection>
		<url>https://code-repo.d4science.org/gCubeSystem/${project.artifactId}</url>
	</scm>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.gcube.distribution</groupId>
				<artifactId>gcube-smartgears-bom</artifactId>
				<version>4.0.0-SNAPSHOT</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
		<!-- smartgears -->
		<dependency>
			<groupId>org.gcube.core</groupId>
			<artifactId>common-smartgears</artifactId>
		</dependency>
		<dependency>
			<groupId>org.gcube.common</groupId>
			<artifactId>common-security</artifactId>
		</dependency>
		<dependency>
			<groupId>org.gcube.common</groupId>
			<artifactId>authorization-control-library</artifactId>
		</dependency>
		<dependency>
			<groupId>org.gcube.core</groupId>
			<artifactId>common-smartgears-app</artifactId>
		</dependency>

		<!-- jersey -->
		<dependency>
			<groupId>org.glassfish.jersey.containers</groupId>
			<artifactId>jersey-container-servlet</artifactId>
		</dependency>

		<!--
		https://mvnrepository.com/artifact/org.glassfish.jersey.inject/jersey-cdi2-se -->
		<dependency>
			<groupId>org.glassfish.jersey.inject</groupId>
			<artifactId>jersey-cdi2-se</artifactId>
		</dependency>
		
		<!-- One of jersey-hk2 or jersey-cdi2-se is needed to allow ResourceIntializer working -->
<!--		<dependency>-->
<!--    		<groupId>org.glassfish.jersey.inject</groupId>-->
<!--    		<artifactId>jersey-hk2</artifactId>-->
<!--		</dependency>-->



		<!-- add jackson as json provider -->
		<dependency>
			<groupId>org.glassfish.jersey.media</groupId>
			<artifactId>jersey-media-json-jackson</artifactId>
		</dependency>

		<!-- logs  -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
		</dependency>

		<!--
		<dependency>
			<groupId>org.aspectj</groupId>
			<artifactId>aspectjrt</artifactId>
		</dependency>
		-->

		<!-- Test Dependencies -->
		<dependency>
			<groupId>org.gcube.common</groupId>
			<artifactId>d4science-iam-client</artifactId>
			<version>[1.0.0-SNAPSHOT, 2.0.0)</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.11</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<build>
		<plugins>

			 
			<!-- OPTIONAL. authorization-control-library: add this plugin if you -->
			<!--	want to use gcube authorization control funzionalities -->
			<!--			<plugin>-->
			<!--				<groupId>org.codehaus.mojo</groupId>-->
			<!--				<artifactId>aspectj-maven-plugin</artifactId>-->
			<!--				<version>${aspectj-plugin.version}</version>-->
			<!--				<configuration>-->
			<!--					<complianceLevel>${java.version}</complianceLevel>-->
			<!--					<source>${java.version}</source>-->
			<!--					<target>${java.version}</target>-->
			<!--					<aspectLibraries>-->
			<!--						<aspectLibrary>-->
			<!--							<groupId>org.gcube.common</groupId>-->
			<!--							<artifactId>authorization-control-library</artifactId>-->
			<!--						</aspectLibrary>-->
			<!--					</aspectLibraries>-->
			<!--				</configuration>-->
			<!--				<executions>-->
			<!--					<execution>-->
			<!--						<goals>-->
			<!--							<goal>compile</goal>-->
			<!--						</goals>-->
			<!--					</execution>-->
			<!--				</executions>-->
			<!--			</plugin>-->


			<!-- Sphinx plugin' -->
			<plugin>
				<groupId>kr.motd.maven</groupId>
				<artifactId>sphinx-maven-plugin</artifactId>
				<version>2.10.0</version>
				<configuration>
					<outputDirectory>
						${project.build.directory}${file.separator}${project.build.finalName}${file.separator}docs</outputDirectory>
					<builder>html</builder>
					<configDirectory>${project.basedir}${file.separator}documentation${file.separator}sphinx</configDirectory>
					<sourceDirectory>${project.basedir}${file.separator}documentation${file.separator}sphinx</sourceDirectory>
					<!-- brew install sphinx-doc -->
					<!-- binaryUrl>file:/opt/homebrew/opt/sphinx-doc/bin/sphinx-build</binaryUrl -->
				</configuration>
				<executions>
					<execution>
						<phase>process-resources</phase>
						<goals>
							<goal>generate</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- Enunciate Maven plugin -->
			<plugin>
				<groupId>com.webcohesion.enunciate</groupId>
				<artifactId>enunciate-maven-plugin</artifactId>
				<version>${enunciate.version}</version>
				<executions>
					<execution>
						<id>assemble</id>
						<goals>
							<goal>assemble</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<!-- This does not works as expected. We need to use the maven-resources-plugin below 
					<docsDir>${project.build.directory}${file.separator}${project.artifactId}-${project.version}${file.separator}api-docs</docsDir>
					-->
					<configFile>${project.basedir}${file.separator}documentation${file.separator}enunciate${file.separator}enunciate.xml</configFile>
				</configuration>
			</plugin>

			<!-- Copy Enunciate Documentation from your-application/api-docs into your war -->
			<plugin>
			    <groupId>org.apache.maven.plugins</groupId>
			    <artifactId>maven-resources-plugin</artifactId>
			    <executions>
			        <execution>
			            <id>copy-enunciate-docs</id>
			            <phase>process-resources</phase>
			            <goals>
			                <goal>copy-resources</goal>
			            </goals>
			            <configuration>
			                <outputDirectory>target</outputDirectory>
			                <resources>
			                    <resource>
			                        <targetPath>${project.build.directory}${file.separator}${project.artifactId}-${project.version}${file.separator}api-docs</targetPath>
			                        <directory>${project.build.directory}/api-docs</directory>
			                        <filtering>true</filtering>
			                    </resource>
			                </resources>
			            </configuration>
			        </execution>
			    </executions>
			</plugin>
			
		</plugins>
	</build>
</project>