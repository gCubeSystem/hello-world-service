package org.gcube.service.helloworld.test;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.service.helloworld.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is a JUnit test example that extends the ContextTest class.
 * It contains a test method that retrieves a secret from the SecretManagerProvider
 * and logs the context and user ID associated with the secret.
 * 
 * <p>Dependencies:
 * <ul>
 *   <li>org.slf4j.Logger</li>
 *   <li>org.slf4j.LoggerFactory</li>
 *   <li>org.junit.Test</li>
 *   <li>SecretManagerProvider</li>
 *   <li>Secret</li>
 * </ul>
 * </p>
 * 
 * <p>Usage:
 * <pre>
 * {@code
 * JUnitTestExample testExample = new JUnitTestExample();
 * testExample.test();
 * }
 * </pre>
 * </p>
 * 
 * Before running the test, make sure you have properly:
 * <ul>
 *  <li>Set up the logging configuration in the logback.xml file</li>
 *  <li>Created the config.ini file in src/test/resources (please check the config.ini.example file as reference)</li>
 * </ul>
 * 
 * 
 * @see ContextTest
 * @see SecretManagerProvider
 * @see Secret
 */
public class JUnitTestExample extends ContextTest {

    private static final Logger logger = LoggerFactory.getLogger(JUnitTestExample.class);

    @Test
    public void test() {
        Secret secret = SecretManagerProvider.get();
        logger.info("Test is executed in context {} for user {}", secret.getContext(), secret.getOwner().getId());
    }

}
