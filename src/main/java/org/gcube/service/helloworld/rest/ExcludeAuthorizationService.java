package org.gcube.service.helloworld.rest;

import org.gcube.service.helloworld.annotation.PURGE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@Path("guest")
public class ExcludeAuthorizationService {

	private final Logger logger = LoggerFactory.getLogger(ExcludeAuthorizationService.class);

	/**
	 * this method doesn't need authorization and the SecretManagerProvider is null
	 * see to implement this behavior add to excludes section in your
	 * application.yaml
	 * 
	 * - path: /{path-to-your-method-path}
	 * 
	 * example for this method
	 * 
	 * - path: /excluded
	 * 
	 */
	@GET
	public String exludedMethod() {
		logger.info("executed whithout any authorization");
		return "executed whithout any authorization";
	}
	
	@GET
	@Path("bad-request")
	public String badRequest() throws WebApplicationException {
		throw new BadRequestException("Your request is not valid");
	}
	
	@GET
	@Path("not-found")
	public String notFound() throws WebApplicationException {
		throw new NotFoundException("Not found sorry");
	}
	
	@PURGE
	@Path("test-purge")
	public Response testPurge() throws WebApplicationException {
		return Response.status(Status.NO_CONTENT).build();
	}
	
}
