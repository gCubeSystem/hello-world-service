package org.gcube.service.helloworld.rest;


import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.service.helloworld.beans.ResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Provider
public class ServiceExceptionMapper implements ExceptionMapper<Exception> {

	private final Logger logger = LoggerFactory.getLogger(ServiceExceptionMapper.class);
	
	@Override
	public Response toResponse(Exception exception) {

		Status status = Status.INTERNAL_SERVER_ERROR;
		String exceptionMessage = exception.getMessage();
		
		try {
			if(exception.getCause() != null) {
				exceptionMessage = exception.getCause().getMessage();
			}
		} catch(Exception e) {
			exceptionMessage = exception.getMessage();
		}
		MediaType mediaType = MediaType.TEXT_PLAIN_TYPE;
		
		if(WebApplicationException.class.isAssignableFrom(exception.getClass())) {
			Response gotResponse = ((WebApplicationException) exception).getResponse();
			status = Status.fromStatusCode(gotResponse.getStatusInfo().getStatusCode());
		}
		
		
		ResponseBean responseBean = new ResponseBean();
		responseBean.setSuccess(false);
		responseBean.setMessage(exceptionMessage);
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			exceptionMessage = objectMapper.writeValueAsString(responseBean);
		} catch (JsonProcessingException e) {
			logger.warn("Error while serializing ResponseBean", e);
		}
		
		return Response.status(status).entity(exceptionMessage).type(mediaType).build();
	}

}
