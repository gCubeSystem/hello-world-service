package org.gcube.service.helloworld.rest;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import org.gcube.common.authorization.control.annotations.AuthorizationControl;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.smartgears.utils.InnerMethodName;

/**
 * @author Lucio Lelii (ISTI - CNR)
 * @author Alfredo Oliviero (ISTI - CNR)
 */

@Path("auth")
public class AuthenticatedService {

	private static final String ALLOWED_ROLE = "myRole";
	private static final String ALLOWED_ROLE_ORG = "OrganizationMember";
	private static final String ALLOWED_ROLE_MEMBER = "Member";

	@AuthorizationControl(allowedRoles = { ALLOWED_ROLE_ORG })
	@GET
	@Path("org_member")
	@Produces(MediaType.TEXT_PLAIN)
	public String authorizedOrg() {
		InnerMethodName.set("auth");
		Secret secret = SecretManagerProvider.get();
		String userId = secret.getOwner().getId();
		String context = secret.getContext();
		return String.format(
				"User %s in context %s is authorized to execute this method because he has the role %s", userId,
				context, ALLOWED_ROLE_ORG);
	}

	@AuthorizationControl(allowedRoles = { ALLOWED_ROLE_MEMBER })
	@GET
	@Path("member")
	@Produces(MediaType.TEXT_PLAIN)
	public String authorizedMember() {
		InnerMethodName.set("auth");
		Secret secret = SecretManagerProvider.get();
		String userId = secret.getOwner().getId();
		String context = secret.getContext();
		return String.format(
				"User %s in context %s is authorized to execute this method because he has the role %s", userId,
				context, ALLOWED_ROLE_MEMBER);
	}

	@AuthorizationControl(allowedRoles = { ALLOWED_ROLE })
	@GET
	@Path("")
	@Produces(MediaType.TEXT_PLAIN)
	public String authorized() {
		InnerMethodName.set("auth");
		Secret secret = SecretManagerProvider.get();
		String userId = secret.getOwner().getId();
		String context = secret.getContext();
		return String.format(
				"User %s in context %s is authorized to execute this method because he has the role %s", userId,
				context, ALLOWED_ROLE);
	}

}
