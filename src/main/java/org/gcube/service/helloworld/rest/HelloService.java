package org.gcube.service.helloworld.rest;

import java.util.HashMap;
import java.util.Map;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.gcube.common.security.Owner;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.service.helloworld.HelloWorldManager;
import org.gcube.service.helloworld.serializers.ContextSerializator;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.smartgears.context.container.ContainerContext;
import org.gcube.smartgears.utils.InnerMethodName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Lucio Lelii (ISTI-CNR)
 * @author Alfredo Oliviero (ISTI-CNR)
 */

@ManagedBy(HelloWorldManager.class)
@Path("")
public class HelloService {

	private final Logger logger = LoggerFactory.getLogger(HelloService.class);

	/**
	 * Hello
	 * @return a string
	 */
	@GET
	@Path("hello")
	@Produces(MediaType.TEXT_PLAIN)
	public String hello() {
		InnerMethodName.set("hello");
		Secret secret = SecretManagerProvider.get();
		String userId = secret.getOwner().getId();
		String context = secret.getContext();

		String infrastructureName = ContextProvider.get().container().configuration().infrastructure();
		logger.info("caller id is {}", userId);
		return String.format("Hello %s in context %s in infastructure %s -roles %s", userId, context,
				infrastructureName, secret.getOwner().getRoles());
	}

	@GET
	@Path("details")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response details() {
		InnerMethodName.set("details");

		Map<String, Object> data = new HashMap<>();

		Secret secret = SecretManagerProvider.get();

		String context = secret.getContext();
		data.put("context", context);

		Owner owner = secret.getOwner();
		data.put("owner", owner);

		ContainerContext container = ContextProvider.get().container();
		data.put("container", container);

		ObjectMapper objectMapper = ContextSerializator.getSerializer();

		try {
			String jsonData = objectMapper.writeValueAsString(data);
			return Response.ok(jsonData).build();

		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

}