package org.gcube.service.helloworld;

import org.gcube.service.helloworld.rest.HelloService;
import org.gcube.smartgears.annotations.ManagedBy;
import org.glassfish.jersey.server.ResourceConfig;

import jakarta.ws.rs.ApplicationPath;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@ApplicationPath("/")
@ManagedBy(HelloWorldManager.class)
public class ResourceInitializer extends ResourceConfig {
	
	public ResourceInitializer() {
		packages(HelloService.class.getPackage().toString());
	}
	
}
