package org.gcube.service.helloworld.beans;

import java.io.Serializable;

/**
 * Response bean
 *
 */
public class ResponseBean implements Serializable {

    private static final long serialVersionUID = -2725238162673879658L;
    /**
     * The result of the request: true if it succeeded, false otherwise
     */
    protected boolean success;

    /**
     * An error message if something wrong happened, null/empty otherwise
     */
    protected String message;
    /**
     * The result object of the request
     */
    protected Object result;

    public ResponseBean() {
        super();
    }

    /**
     * @param success
     * @param message
     * @param result
     */
    public ResponseBean(boolean success, String message, Object result) {
        super();
        this.success = success;
        this.message = message;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "ResponseBean [success=" + success
                + ", message=" + message + ", result=" + result + "]";
    }
}
