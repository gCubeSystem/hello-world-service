package org.gcube.service.helloworld.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.common.security.Owner;
import org.gcube.smartgears.context.container.ContainerContext;

/**
 * 
 * @author Lucio Lelii (ISTI-CNR)
 */

public class RestUtils {

    public static Map<String, Object> getUserDict(Owner owner) {
        String userId = owner.getId();
        String clientName = owner.getClientName();

        String clientId = owner.getId();
        List<String> roles = owner.getRoles();
        String email = owner.getEmail();
        String firstName = owner.getFirstName();
        String lastName = owner.getLastName();
        boolean externalClient = owner.isExternalClient();

        String contactPerson = owner.getContactPerson();
        String contactOrganisation = owner.getContactOrganisation();

        Map<String, Object> data = new HashMap<>();
        data.put("userid", userId);
        data.put("clientName", clientName);
        data.put("clientId", clientId);
        data.put("roles", roles);
        data.put("email", email);
        data.put("firstName", firstName);
        data.put("lastName", lastName);
        data.put("externalClient", externalClient);
        data.put("contactPerson", contactPerson);
        data.put("contactOrganisation", contactOrganisation);
        return data;
    }

    public static Map<String, Object> getContainerDict(ContainerContext container) {
        Map<String, Object> data = new HashMap<>();
        data.put("id", container.id());
        data.put("configuration", container.configuration());
        // data.put("lifecycle", container.lifecycle());
        data.put("properties", container.properties());
        data.put("authorizationProvider", container.authorizationProvider());
        return data;
    }
}
