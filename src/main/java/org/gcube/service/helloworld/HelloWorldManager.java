package org.gcube.service.helloworld;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.smartgears.ApplicationManager;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.configuration.Mode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This class is used to Manage the application initialization and shutdown per
 * context; The init and shutdown methods are called one per context in which
 * the app is running respectively at init and a shutdown time. It is connected
 * to the app declaring it via the @ManagedBy annotation (@see ResourceInitializer)
 * 
 * @author Lucio Lelii (ISTI-CNR)
 */
public class HelloWorldManager implements ApplicationManager {

	Logger logger = LoggerFactory.getLogger(HelloWorldManager.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onInit() {
		if (ContextProvider.get().container().configuration().mode() == Mode.offline) {
			logger.debug("init called in offline mode");
		} else {
			Secret secret = SecretManagerProvider.get();
			if (secret != null) {
				logger.debug("init called in context {}", secret.getContext());
			} else {
				logger.debug("init called in null context");
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onShutdown() {
		if (ContextProvider.get().container().configuration().mode() == Mode.offline) {
			logger.debug("shutDown called in offline mode");
		} else {
			Secret secret = SecretManagerProvider.get();
			if (secret != null) {
				logger.debug("shutDown called in context {}", secret.getContext());
			} else {
				logger.debug("shutDown called in null context");
			}
		}
	}
}
