package org.gcube.service.helloworld.health;

import java.io.File;
import java.io.IOException;

import org.gcube.common.health.api.HealthCheck;
import org.gcube.common.health.api.ReadinessChecker;
import org.gcube.common.health.api.response.HealthCheckResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * this class is used to add checks about components used by the service (DB,
 * external FS, other services etc. )
 * and automatically exposed using the REST method webapp/gcube/resources/health
 * 
 * 
 * @author lucio
 *
 */

@ReadinessChecker
public class CreateTempFileCheck implements HealthCheck {

	private static Logger log = LoggerFactory.getLogger(CreateTempFileCheck.class);

	@Override
	public String getName() {
		return "create temp file";
	}

	@Override
	public HealthCheckResponse check() {
		try {
			File.createTempFile("exampleTest", "txt");
			return HealthCheckResponse.builder(getName()).up().info("health check example").build();
		} catch (IOException e) {
			log.error("error checking defaultStorage", e);
			return HealthCheckResponse.builder(getName()).down().error(e.getMessage()).build();
		}

	}

}