package org.gcube.service.helloworld.serializers;

import java.io.IOException;

import org.gcube.smartgears.context.container.ContainerContext;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * Jackson serializer for ContainerContext
 * 
 * @author Alfredo Oliviero (ISTI-CNR)
 * 
 */

public class ContainerContextSerializer extends StdSerializer<ContainerContext> {
    protected ContainerContextSerializer(Class<ContainerContext> t) {
        super(t);
    }

    public ContainerContextSerializer() {
        super(ContainerContext.class, true);
    }

    @Override
    public void serialize(ContainerContext ccontext, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {
        jgen.writeStartObject();
        jgen.writeStringField("id", ccontext.id());
        // jgen.writeObjectField("configuration.site", ccontext.configuration().site());
        // jgen.writeObjectField("configuration", ccontext.configuration());
        jgen.writeObjectField("properties", ccontext.properties());
        jgen.writeObjectField("authorizationProvider", ccontext.authorizationProvider());

        jgen.writeObjectField("configuration", ccontext.configuration());
        jgen.writeObjectField("desc", ccontext.toString());

        jgen.writeEndObject();
    }
}
