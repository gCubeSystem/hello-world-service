package org.gcube.service.helloworld.serializers;

import java.io.IOException;

import org.gcube.smartgears.configuration.container.ContainerConfiguration;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * Jackson serializer for ContainerConfiguration
 * 
 * @author Alfredo Oliviero (ISTI-CNR)
 * 
 */

public class ContainerConfigurationSerializer extends StdSerializer<ContainerConfiguration> {
    protected ContainerConfigurationSerializer(Class<ContainerConfiguration> t) {
        super(t);
    }

    public ContainerConfigurationSerializer() {
        super(ContainerConfiguration.class, true);
    }

    @Override
    public void serialize(ContainerConfiguration configuration, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {
        jgen.writeStartObject();
        jgen.writeObjectField("mode", configuration.mode());
        jgen.writeObjectField("app", configuration.apps());
        jgen.writeObjectField("site", configuration.site());
        jgen.writeObjectField("infrastructure", configuration.infrastructure());
        jgen.writeObjectField("hostname", configuration.hostname());
        jgen.writeObjectField("port", configuration.port());
        jgen.writeObjectField("protocol", configuration.protocol());
        jgen.writeObjectField("authorizeChildrenContext", configuration.authorizeChildrenContext());
        jgen.writeObjectField("proxy", configuration.proxy());

        jgen.writeObjectField("desc", configuration.toString());

        jgen.writeEndObject();
    }
}
