package org.gcube.service.helloworld.serializers;

import java.io.IOException;

import org.gcube.smartgears.security.SimpleCredentials;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * Jackson serializer for SimpleCredentials
 * 
 * @author Alfredo Oliviero (ISTI-CNR)
 * 
 */

public class SimpleCredentialsSerializer extends StdSerializer<SimpleCredentials> {

    protected SimpleCredentialsSerializer(Class<SimpleCredentials> t) {
        super(t);
    }

    public SimpleCredentialsSerializer() {
        super(SimpleCredentials.class, true);
    }

    @Override
    public void serialize(SimpleCredentials credentials, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {
        jgen.writeStartObject();
        jgen.writeStringField("clientId", credentials.getClientID());
        jgen.writeStringField("secret", "[*****]");
        jgen.writeEndObject();
    }
}
