package org.gcube.service.helloworld.serializers;

import java.io.IOException;

import org.gcube.common.security.Owner;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * Jackson serializer for Owner
 * 
 * @author Alfredo Oliviero (ISTI-CNR)
 * 
 */

public class OwnerSerializer extends StdSerializer<Owner> {

    protected OwnerSerializer(Class<Owner> t) {
        super(t);
    }

    public OwnerSerializer() {
        super(Owner.class, true);
    }

    @Override
    public void serialize(Owner owner, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeStartObject();
        jgen.writeStringField("ownerId", owner.getId());
        jgen.writeStringField("clientName", owner.getClientName());
        jgen.writeArrayFieldStart("roles");
        for (String role : owner.getRoles()) {
            jgen.writeString(role);
        }
        jgen.writeEndArray();
        jgen.writeStringField("email", owner.getEmail());
        jgen.writeStringField("firstName", owner.getFirstName());
        jgen.writeStringField("lastName", owner.getLastName());
        jgen.writeBooleanField("externalClient", owner.isExternalClient());
        jgen.writeStringField("contactPerson", owner.getClientName());
        jgen.writeStringField("contactOrganisation", owner.getContactOrganisation());

        jgen.writeEndObject();
    }
}
