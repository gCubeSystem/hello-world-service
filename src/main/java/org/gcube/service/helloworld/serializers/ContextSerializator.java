package org.gcube.service.helloworld.serializers;

import org.gcube.smartgears.configuration.container.ContainerConfiguration;
import org.gcube.smartgears.context.container.ContainerContext;
import org.gcube.smartgears.security.SimpleCredentials;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * Jackson Serialization utils for Smartgear Context classes
 * 
 * @author Alfredo Oliviero (ISTI-CNR)
 * 
 */

public class ContextSerializator {
    private static ObjectMapper serializer = null;

    public static ObjectMapper getSerializer() {
        if (serializer == null) {
            ObjectMapper om = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            // module.addSerializer(Owner.class, new OwnerSerializer());

            module.addSerializer(ContainerConfiguration.class, new ContainerConfigurationSerializer());
            module.addSerializer(ContainerContext.class, new ContainerContextSerializer());
            module.addSerializer(SimpleCredentials.class, new SimpleCredentialsSerializer());

            om.registerModule(module);
            serializer = om;
        }
        return serializer;
    }

}
