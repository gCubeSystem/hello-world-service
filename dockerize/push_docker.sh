#!/bin/sh

set -a
# source build_conf > docker.conf # generate current conf
# source docker.conf
source build_conf

echo "Docker image $HARBOR_IMAGE_NAME"

docker push $HARBOR_IMAGE_NAME

set +a
