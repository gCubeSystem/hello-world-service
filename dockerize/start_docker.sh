#!/bin/sh

set -a
# source build_conf > docker.conf # generate current conf
# source docker.conf
source build_conf

echo "Docker image $HARBOR_IMAGE_NAME"
docker pull $HARBOR_IMAGE_NAME
docker run -d -p 9991:8080 --name $NAME $HARBOR_IMAGE_NAME

set +a
