#!/bin/bash
REGISTRY_URL="hub.dev.d4science.org"
#USERNAME="alfredo.oliviero"
echo "to obtain Harbor username and CLI secret:"
echo "https://hub.dev.d4science.org/ -> user profile -> CLI secret"

read -p "username:" USERNAME

echo ""

read -s -p "CLI secret:" ACCESS_TOKEN
echo "$ACCESS_TOKEN" | docker login $REGISTRY_URL -u $USERNAME --password-stdin
unset ACCESS_TOKEN
