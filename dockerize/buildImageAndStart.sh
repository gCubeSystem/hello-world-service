#!/bin/bash

# set -x   # uncomment to debug script
set -a
source ./build_conf
set +a

ACCEPTED_JAVA_VERSIONs=(11 17)
PORT=8080
DEBUG_PORT=8100
DEBUG=false
EXECUTE=false
TEST=false
COMPILE=true

GOAL="clean package"

PUSH_DOCKER=false
PUSH_HARBOR=false
LOGIN_HARBOR=false

################################################################################
# Help                                                                         #
################################################################################
Help() {
    # Display Help
    echo "build, create and run in docker the identity manager service"
    echo
    echo "Syntax: buildDistribution "
    echo "options:"

    echo "-e        execute the docker image"
    echo "-g arg    specifies the maven [g]oals {package, install, deploy etc} default is $GOAL."
    echo "-d arg?   enable java debug mode for execution"
    echo "          arg is the debug port (default is $DEBUG_PORT)"

    echo "-r        push image to d4science harbo[r] (with login already done, or -l to login)"
    echo "-u        p[u]sh image to dockerhub (with docker login already done)"

    echo "-p arg    specifies the port to be exposed for the docker container to access the service (default $PORT)"

    echo "-c arg     path of the file to deploy as container.ini (default ./docker/container.ini)"

    # echo "-j arg    specify java version (default is $JAVA_VERSION)"
    # echo "          accepted version are: ${ACCEPTED_JAVA_VERSIONs[@]}"

    echo "-s        skip maven execution"
    echo "-t        exec also maven tests"

    echo "-n arg    specifies the docker image name (default is $MVN_NAME)."

    echo "-l        [l]ogin to d4science harbor"
    echo "-h        Print this Help."
    echo
    echo "to compile and push to harbor registry with a custom container.ini file: "
    echo "     ./buildImageAndStart.sh -r -m  -l -c \"./docker/container-XXX.ini\" "
    echo
    echo "to debug locally: "
    echo "     ./buildImageAndStart.sh -d "
}

################################################################################
################################################################################
# Main program                                                                 #
################################################################################
################################################################################

set -e

#OPTSTRING=":c:n:p:g:d:?jsmulrteh"
OPTSTRING=":c:n:p:g:d:?smulrteh"

while getopts $OPTSTRING opt; do
    # echo "Option -${opt} was triggered, Argument: ${OPTARG}"
    case "${opt}" in
    s) COMPILE=false && echo "compile $COMPILE" ;;
    g) GOAL=${OPTARG} ;;
    c)
        CONTAINER_INI=${OPTARG}
        echo "CONTAINER_INI: $CONTAINER_INI"
        ;;
    m) MULTI_PLATFORM=true ;;
    n) NAME=${OPTARG} ;;
    p) PORT=${OPTARG} ;;

    u) PUSH_DOCKER=true ;;
    l) LOGIN_HARBOR=true ;;
    r) PUSH_HARBOR=true ;;

    t) TEST=true ;;
    e) EXECUTE=true ;;
    d)
        DEBUG=true
        DEBUG_PORT=${OPTARG}
        echo "debug enabled, port $DEBUG_PORT, execute $EXECUTE"
        ;;
    # j)
    #     if [[ ${ACCEPTED_JAVA_VERSIONs[@]} =~ ${OPTARG} ]]; then
    #         JAVA_VERSION=${OPTARG}
    #     else
    #         echo "Invalid java version" && echo "accepted version are: ${ACCEPTED_JAVA_VERSIONs[@]}" && exit 1
    #     fi
    #     ;;
    h) Help && exit 0 ;;

    :)
        # matched when an option that is expected to have an argument is passed without one
        if [ ${OPTARG} = "d" ]; then
            DEBUG=true
            EXECUTE=true
            echo "debug enabled, port $DEBUG_PORT"
        else
            # matched when an option that is expected to have an argument is passed without one
            echo "Option -${OPTARG} requires an argument."
            exit 1
        fi
        ;;
    ?) # match any invalid option that is passed
        echo "Invalid option: -${OPTARG}."
        exit 1
        ;;
    esac
done

if [ $COMPILE = true ]; then
    SKIP_TEST=""
    if [ $TEST = false ]; then
        SKIP_TEST="-Dmaven.test.skip"
    fi

    ( cd .. && mvn $GOAL $SKIP_TEST );
else
    echo "skipping mvn package"
fi

echo "SMARTGEAR IMAGE: $SMARTGEAR_IMAGE"
docker pull $SMARTGEAR_IMAGE

if [ $MULTI_PLATFORM ]; then
    PLATFORMS="--platform=linux/amd64,linux/arm64,linux/arm/v7"
    echo "build multiple platform $PLATFORMS"
fi

echo "DOCKER_BUILD_NAME: $DOCKER_BUILD_NAME"

( cd .. && docker build -t $DOCKER_BUILD_NAME \
    --build-arg="MVN_FINALNAME=$MVN_FINALNAME" \
    --build-arg="MVN_NAME=${MVN_NAME}" \
    --build-arg="CONTAINER_INI=${CONTAINER_INI}" \
    --build-arg="SMARTGEAR_IMAGE=${SMARTGEAR_IMAGE}" \
    --build-arg="PORT=${PORT}" \
    -f Dockerfile \
    $PLATFORMS . );

if [ ${PUSH_DOCKER} = true ]; then
    docker tag $DOCKER_BUILD_NAME $DOCKER_IMAGE_NAME
    docker push $DOCKER_IMAGE_NAME
    echo ">>> pushed on dockerhub the image $DOCKER_IMAGE_NAME"
fi

if [ ${LOGIN_HARBOR} = true ]; then
    ./loginHarborHub.sh
fi

if [ $PUSH_HARBOR = true ]; then
    echo ">>> PUSHING on hub.dev.d4science.org the image $HARBOR_IMAGE_NAME"

    docker tag $DOCKER_BUILD_NAME $HARBOR_IMAGE_NAME
    echo ">>>    docker push $HARBOR_IMAGE_NAME"
    docker push $HARBOR_IMAGE_NAME
    echo ">>> pushed on hub.dev.d4science.org the image $HARBOR_IMAGE_NAME"
fi

if [ ${EXECUTE} = true ]; then
    if [ $DEBUG = true ]; then
        docker run -p $PORT:8080 -p $DEBUG_PORT:8100 -e JAVA_TOOL_OPTIONS="-agentlib:jdwp=transport=dt_socket,address=*:8100,server=y,suspend=y" $DOCKER_BUILD_NAME
    else
        docker run -p $PORT:8080 $DOCKER_BUILD_NAME
    fi
fi
