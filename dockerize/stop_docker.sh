#!/bin/sh

set -a
# source build_conf > docker.conf # generate current conf
# source docker.conf
source build_conf

docker stop $NAME
docker rm $NAME

set +a
