#!/bin/sh

set -a
# source build_conf > docker.conf # generate current conf
# source docker.conf
source build_conf

echo "Docker Smartgear image $SMARTGEAR_IMAGE"

# docker pull hub.dev.d4science.org/gcube/smartgears-distribution:4.0.1-SNAPSHOT-java11-tomcat10.1.19

docker pull $SMARTGEAR_IMAGE

set +a
