# Instructions

prerequisites:

1. JDK 11 or 17 (recommended). See [macos_jdk.md](macos_jdk.md) for installation instructions on MacOS
2. maven. See [macos_jdk.md](macos_jdk.md) for installation instructions on MacOS
3. installed maven-parent 1.2 See [maven-parent](https://gcube.wiki.gcube-system.org/gcube/Maven_Configuration) )
4. if on MacOS with M1 chip, a patch for sphinx is needed. See [macos_sphinx.md](macos_sphinx.md) for installation instructions on MacOS

## container.ini configuration file

Smartgears4 needs a container.ini file to configure the service.

to execute the service in a docker container, the container.ini file is expected in the dockerize/configuration folder (./dockerize/configuration/container.ini)

an example of the file (container.default.ini) is provided in the same folder.

just copy the file to ./dockerize/configuration/container.ini and add the credentials.clientID and credentials.secret values to interact with the Keycloak server.

## quick start

to quick start an instance of the service in a docker container, execute the following steps:

* ensure you provided a ./dockerize/configuration/container.ini file with the correct configuration, es described in the previous section

```sh
java17
mvn clean package -U
cd dockerize && ./buildImageAndStart.sh -s -e

```

in a different terminal, check if you can access the service

```sh
curl -l "http://localhost:8080/hello-world-service/gcube/resource/metrics"
curl -l "http://localhost:8080/hello-world-service/gcube/resource/health"
curl -l 'http://localhost:8080/hello-world-service/guest'
```

## Docker Execution

```sh
start the docker container
```sh
cd dockerize && ./buildImageAndStart.sh -e
```

debug the docker container (read [### DEBUG] for details)

```sh
cd dockerize && ./buildImageAndStart.sh -d
```

to compile and push to harbor registry with a custom container.ini file: 

```sh
cd dockerize && ./buildImageAndStart.sh -r -m  -l -c "./docker/container-XXX.ini"
```

### Test URLs

guest access (no token required)

```sh
curl -l 'http://localhost:8080/hello-world-service/guest'
```

metrics:

```sh
curl -l "http://localhost:8080/hello-world-service/gcube/resource/metrics"
curl -l "http://localhost:8080/hello-world-service/gcube/resource/health"
```

authenticated access

obtain OAuth Access Token at https://next.dev.d4science.org/group/gcube/home
replace {{TOKEN}} with your token

```sh
TOKEN={{TOKEN}}

curl -l -H "Authorization: Bearer $TOKEN" "http://localhost:8080/hello-world-service/hello"

curl -l -H "Authorization: Bearer $TOKEN" "http://localhost:8080/hello-world-service/details"

curl -l -H "Authorization: Bearer $TOKEN" "http://localhost:8080/hello-world-service/auth"

curl -l -H "Authorization: Bearer $TOKEN" "http://localhost:8080/hello-world-service/org_member"

curl -l -H "Authorization: Bearer $TOKEN" "http://localhost:8080/hello-world-service/member"
```

### Debug

start the docker container in debug Mode
```cd dockerize && ./buildImageAndStartWithDebug.sh -d```

connect the JAVA debugger to port 5005

* VSCODE: press the green play button and start "Debug (Attach)


## Project Structure

```sh
│
# gitignore configuration
│ .gitignore
│
# readme, license, instruction files
├── CHANGELOG.md
├── FUNDING.md
├── Instructions.md
├── LICENSE.md
├── README.md
│ 
# maven configuration for the project
├── pom.xml
│
# the files in the gcube folder are processed by mvn parent during the packing fase  
# the variables ${VAR} are resolved using the values found in pom.xml 
# and the generated file are copied (respecting the gcube folder structure) inside the folder generated in ./target and so in the corrisponding war file
├── gcube
│   └── extra-resources
│       └── WEB-INF
#           #  defines the smartgear application configuration
│           ├── application.yaml
│           └── web.xml
| 
# project sources
├── src
│   ├── main
│   │   ├── java
│   │   │   └── org
│   │   │       └── gcube
│   │   │           └── service
│   │   │               └── helloworld
│   │   │                   ├── HelloWorldManager.java
│   │   │                   ├── ResourceInitializer.java
│   │   │                   ├── annotation
│   │   │                   │   └── PURGE.java
│   │   │                   ├── beans
│   │   │                   │   └── ResponseBean.java
│   │   │                   ├── health
│   │   │                   │   └── CreateTempFileCheck.java
│   │   │                   ├── rest
│   │   │                   │   ├── AuthenticatedService.java
│   │   │                   │   ├── ExcludeAuthorizationService.java
│   │   │                   │   ├── HelloService.java
│   │   │                   │   └── ServiceExceptionMapper.java
│   │   │                   ├── serializers
│   │   │                   │   ├── ContainerConfigurationSerializer.java
│   │   │                   │   ├── ContainerContextSerializer.java
│   │   │                   │   ├── ContextSerializator.java
│   │   │                   │   ├── OwnerSerializer.java
│   │   │                   │   └── SimpleCredentialsSerializer.java
│   │   │                   └── utils
│   │   │                       └── RestUtils.java
│   │   ├── resources
│   │   │   └── META-INF
│   │   │       └── enunciate
│   │   │           └── d4science_docs.fmt
│   │   └── webapp
#   #   #   # WEB-INF resources are accessible to the resource loader of your Web-Application 
#   #   #   # and not served directly to a client by the container
│   │       ├── WEB-INF
│   │       └── api-docs
│   │           └── css
│   │               └── d4science_enunciate_custom.css
#   #           # postman collections to test the API
│   │           └── postman
│   │               ├── hello-world-service-for-devvre.postman_environment.json
│   │               └── hello-world-service.postman_collection.json
│   └── test
│       ├── java
│       └── resources
│
# optional, for local develop in docker containers
├── dockerize
│   ├── buildImageAndStart.sh
│   ├── build_conf
│   ├── configuration
│   │   ├── container.default.ini
│   │   ├── container.ini
│   │   └── logback.xml
│   ├── loginHarborHub.sh
│   ├── pull_docker.sh
│   ├── pull_smartgear.sh
│   ├── push_docker.sh
│   ├── start_docker.sh
│   └── stop_docker.sh
|
# documentation config
├── documentation
│   ├── TODO.md
│   ├── dockerizing.md
#   # optional, enunciate configuration
│   ├── enunciate
│   │   └── enunciate.xml
#   # optional, sphinx configuration
│   └── sphinx
│       ├── Makefile
│       ├── conf.py
│       ├── index.rst
│       ├── make.bat
│       └── tests.rst
|
# Docker file
├── Dockerfile
│
# optional, vscode debug configuration 
├── .vscode
│   ├── launch.json
│   └── container.dev.ini
│
# the output of maven package
└── target

```

### file details
* gitignore: gitignore file for java projects

* pom.xml
  * <parent>: maven-parent configuration. currently 1.2.0 version
  * <properties><webappDirectory> : path for web.xml 
  * <scm> ???
  * <dependencyManagement> gcube dependency manager

* dockerize/configuration folder: folder with the configurations for the execution of the application. are copied in the docker container during building

* dockerize/configuration/container.ini: configurations for the docker container. 
  * create your own configuration copying an existing template 
  * the default execution uses dockerize/configuration/container.ini, it can be a simlink to a custom config
  * gitignore ignores all container*.ini files in the folder excepting container.default.ini
  * 
  * mode: [online/offline]: register the service on IS. use offline for local docker instances
  
* gcube folder: the files in the gcube folder are processed by maven-parent, the variables ${VAR} ara resolved using the values found in pom.xml and the generated file are put in the target folder and in the war file.
