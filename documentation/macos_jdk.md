# JDK on macos

ref. https://stackoverflow.com/a/68105964/2473953

this guide will drive you through:

1. the process of installing and managing multiple Java versions on macOS using Homebrew
2. the configuration of a quick way to switch between different versions of Java.

you will install all the needed JDK version, define a default one, and define aliases to switch between different versions of Java , just typing in the bash

* `java8` to switch to JDK 1.8
* `java11` to switch to JDK 11
* `java17` to switch to JDK 17
* `java21` to switch to JDK 21
* `java23` to switch to JDK 23


JDKs install and shell configuration:

1. if not already installed, install Homebrew

    On macos, Homebrew is the best way to manage and work with different Java versions.

    ```sh
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    ```

2. Uninstall AdoptOpenJDK/openjdk

    Homebrew tap AdoptOpenJDK/openjdk is officially deprecated in favor of the temurin casks provided directly from the Homebrew project itself.

    In case you already have Homebrew and AdoptOpenJDK/openjdk cast installed, please untap this brew tap first

    ```sh
    brew untap AdoptOpenJDK/openjdk
    ```

    and remove all the installed versions:

    ```sh
    brew uninstall --cask adoptopenjdk8
    ```

    if maven dependency blocks the brew removal, just remove also maven we will reinstall it later

3. Install all the java version you need with temurin:

    ```sh
    # install the latest version of Java
    brew install --cask temurin

    # install other versions:
    brew install --cask temurin@8
    brew install --cask temurin@11
    brew install --cask temurin@17
    brew install --cask temurin@21 # long term support
    brew install --cask temurin # java23, latest version

    ```

4. list all installed versions:

    ```sh
    /usr/libexec/java_home -V 
    ```

    you should have an output like this:

    ```sh
    /usr/libexec/java_home -V 

    Matching Java Virtual Machines (5):
        23.0.2 (arm64) "Eclipse Adoptium" - "OpenJDK 23.0.2" /Library/Java/JavaVirtualMachines/temurin-23.jdk/Contents/Home
        21.0.6 (arm64) "Eclipse Adoptium" - "OpenJDK 21.0.6" /Library/Java/JavaVirtualMachines/temurin-21.jdk/Contents/Home
        17.0.14 (arm64) "Eclipse Adoptium" - "OpenJDK 17.0.14" /Library/Java/JavaVirtualMachines/temurin-17.jdk/Contents/Home
        11.0.26 (arm64) "Eclipse Adoptium" - "OpenJDK 11.0.26" /Library/Java/JavaVirtualMachines/temurin-11.jdk/Contents/Home
        1.8.0_442 (x86_64) "Eclipse Temurin" - "Eclipse Temurin 8" /Library/Java/JavaVirtualMachines/temurin-8.jdk/Contents/Home
    /Library/Java/JavaVirtualMachines/temurin-23.jdk/Contents/Home
    ```

    if you have some old or unexpected versions, (es    `1.8.0_292 (x86_64) "AdoptOpenJDK" - "AdoptOpenJDK 8" /Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home` ) you can just delete them with `sudo rm -rf`:

    ```sh
    sudo rm /Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk -rf
    ```

5. Switch between different versions of Java

    In this case, we want to be able to switch between Java8, Java11, Java17, Java21 and Java23

    We only need to add the following to your `~/.bash_profile` and/or `~/.zshrc`.

    ```sh
    ############################################

    # java JDK switch management

    ## ref: https://stackoverflow.com/a/68105964/2473953

    ## list installed JVM
    ## /usr/libexec/java_home -V 

    ## set java home versions
    export JAVA_8_HOME=$(/usr/libexec/java_home -v1.8)
    export JAVA_11_HOME=$(/usr/libexec/java_home -v11)
    export JAVA_17_HOME=$(/usr/libexec/java_home -v17)
    export JAVA_21_HOME=$(/usr/libexec/java_home -v21)
    export JAVA_23_HOME=$(/usr/libexec/java_home -v23)


    ## aliases to switch between different versions of Java
    alias java8='export JAVA_HOME=$JAVA_8_HOME; java -version'
    alias java11='export JAVA_HOME=$JAVA_11_HOME; java -version'
    alias java17='export JAVA_HOME=$JAVA_17_HOME; java -version'
    alias java21='export JAVA_HOME=$JAVA_21_HOME; java -version'
    alias java23='export JAVA_HOME=$JAVA_23_HOME; java -version'

    ## set default to Java 17
    java17

    ############################################
    ```

    Reload .bash_profile or .zshrc for the aliases to take effect:

    `$ source ~/.bash_profile`
    or

    `$ source ~/.zshrc`

6. Finally you can use the aliases to switch between different Java versions.

    ```sh
    java8
    java -version

    >> openjdk version "23.0.2" 2025-01-21
    >> OpenJDK Runtime Environment Temurin-23.0.2+7 (build 23.0.2+7)
    >> OpenJDK 64-Bit Server VM Temurin-23.0.2+7 (build 23.0.2+7, mixed mode, sharing)
    ```

    ```sh
    java11
    java -version

    >> openjdk version "11.0.26" 2025-01-21
    >> OpenJDK Runtime Environment Temurin-11.0.26+4 (build 11.0.26+4)
    >> OpenJDK 64-Bit Server VM Temurin-11.0.26+4 (build 11.0.26+4, mixed mode)
    ```

    ```sh
    java17
    java -version

    >> openjdk version "17.0.14" 2025-01-21
    >> OpenJDK Runtime Environment Temurin-17.0.14+7 (build 17.0.14+7)
    >> OpenJDK 64-Bit Server VM Temurin-17.0.14+7 (build 17.0.14+7, mixed mode, sharing)
    ```

    ```sh
    java21
    java -version

    >> openjdk version "23.0.2" 2025-01-21
    >> OpenJDK Runtime Environment Temurin-23.0.2+7 (build 23.0.2+7)
    >> OpenJDK 64-Bit Server VM Temurin-23.0.2+7 (build 23.0.2+7, mixed mode, sharing)
    ```

    ```sh
    java23
    java -version

    >> openjdk version "23.0.2" 2025-01-21
    >> OpenJDK Runtime Environment Temurin-23.0.2+7 (build 23.0.2+7)
    >> OpenJDK 64-Bit Server VM Temurin-23.0.2+7 (build 23.0.2+7, mixed mode, sharing)
    ```

7. Install Maven

   once installed java, just install maven with the brew install

   ```sh
   brew install maven
   ```