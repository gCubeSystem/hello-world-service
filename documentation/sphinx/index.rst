***********************************************************
Welcome to Hello World Service documentation
***********************************************************

Hello World Service is a RESTful application that exposes operations via REST-API.

See the available `REST-API docs <../api-docs/index.html>`_.


Authorization
=============

D4Science adopts state-of-the-art industry standards for authentication and authorization. 
Specifically, the implementation fully adopts `OIDC (OpenID Connect) <https://openid.net/connect>`_ for authentication and UMA 2 (User-Managed Authorization) for authorization flows. 
`JSON Web Token (JWT) Access token <https://jwt.io/>`_ are used for both authentication and authorization.

Obtain your Bearer token here: https://dev.d4science.org/how-to-access-resources

Service
=======

You can call the methods of the Web Service by writing your REST client application or using existing REST client plugins.


HTTP Statuses
-------------

Any successful operation returns a *200 OK* HTTP status code.
The create operation returns *201 Created*. 
Any Background operation returns *202 Accepted*.
Any operation that does not provide any content returns *204 No Content*.



The most common error statuses a client can obtain are:

* **400 Bad Request** used to indicate a clients error `<https://tools.ietf.org/html/rfc7231#section-6.5.1>`_;
* **401 Unauthorized** used to indicate that the client does not provide the authorization token in the HTTP Header or the client does not have enough right to perform such request `<https://tools.ietf.org/html/rfc7235#section-3.1>`_;
* **404 Not Found** used to indicate that the requested instance does not exist `<https://tools.ietf.org/html/rfc7231#section-6.5.4>`_;
* **405 Method Not Allowed** the used HTTP method is not supported for the requested URL `<https://tools.ietf.org/html/rfc7231#section-6.5.5>`_.
  The response contains the *Allow* HTTP Header indicating the supported HTTP method for such URL `<https://tools.ietf.org/html/rfc7231#section-7.4.1>`_;
* **409 Conflict**  the request could not be completed due to a conflict with the current state of the target resource (e.g. the name of the resource already exists) `<https://tools.ietf.org/html/rfc7231#section-6.5.8>`_;
* **500 Internal Server Error** indicate a server failure `<https://tools.ietf.org/html/rfc7231#section-6.6.1>`_.

You can find a complete list of HTTP Status at `<https://httpstatuses.com/>`_ or `<https://httpstatuses.io/>`_

If you get a *500 Internal Server Error*, please report it in the `gCube ticketing system <https://support.d4science.org>`_. 

Please use this checklist before reporting an error:

* Replicate the request;
* The failure could be temporal due to a network error, a server issue, and many other temporal issues. For this reason, please retry the request after a certain amount of time before reporting the issue;
* indicate how to replicate the error;
* indicate the time when the error occurred (this simplifies identifying the issue).

HTTP Methods
------------

Hello World Service is a pure RESTful service. It uses standard HTTP Methods to perform a listing of collections and CRUD (Create Read Update Delete) operations on instances.


.. table::

	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	|  Operation   | HTTP Method | URL                                    | Success HTTP Status |  Safe  | Idempotent |
	+==============+=============+========================================+=====================+========+============+
	| Supported    | OPTIONS     | /{COLLECTION}                          | 204 No Content      |   Y    |     Y      |
	| HTTP Methods |             |                                        | [#allow]_           |        |            |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| List         | GET         | /{COLLECTION}                          | 200 OK              |   Y    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Count        | GET         | /{COLLECTION}?count=true               | 200 OK              |   Y    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Exists       | HEAD        | /{COLLECTION}                          | 204 No Content      |   Y    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Create       | POST        | /{COLLECTION}                          | 201 Created         |   N    |     N      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Supported    | OPTIONS     | /{COLLECTION}/{INSTANCE_ID}            | 204 No Content      |   Y    |     Y      |
	| HTTP Methods |             |                                        | [#allow]_           |        |            |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Exist        | HEAD        | /{COLLECTION}/{INSTANCE_ID}            | 204 No Content      |   Y    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Read         | GET         | /{COLLECTION}/{INSTANCE_ID}            | 200 OK              |   Y    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Update       | PUT         | /{COLLECTION}/{INSTANCE_ID}            | 200 OK              |   N    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Patch        | PATCH       | /{COLLECTION}/{INSTANCE_ID}            | 200 OK              |   N    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Delete       | DELETE      | /{COLLECTION}/{INSTANCE_ID}            | 204 No Content      |   N    |  N [#del]_ |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Purge        | DELETE      | /{COLLECTION}/{INSTANCE_ID}?purge=true | 204 No Content      |   N    |  N [#del]_ |
	+              +-------------+----------------------------------------+---------------------+--------+------------+
	|              | PURGE       | /{COLLECTION}/{INSTANCE_ID}            | 204 No Content      |   N    |  N [#del]_ |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+

.. [#allow] Supported HTTP Methods in **Allow** HTTP Header
	
.. [#del] DELETE has been defined as idempotent.
	
	*Allamaraju* [#Allamaraju]_ argues that DELETE idempotency should be accomplished client-side.
	The server should inform the client if the delete operation succeeded because the resource was deleted or it was not found, i.e., **404 Not Found** error is suggested instead of **204 No Content**.
	The latter situation should be treated as idempotent by the client.
	
	We share the same vision. For this reason, Hello World Service does not provide server-side idempotency for DELETE and PURGE operations.

.. [#Allamaraju] Allamaraju S. RESTful Web Services Cookbook: Solutions for Improving Scalability and Simplicity. O’Reilly. first ed. 2010
 
 
About URL
^^^^^^^^^

The presented URL uses the following convention:

* **{COLLECTION}** is the plural name of the entity type;
* **{INSTANCE_ID}** is an identification that enables univocally identifying the instance in the collection.


About Safety and Idempotency properties
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* A method is *Safe* if it does not produce any side effects.
  "This does not prevent an implementation from including behaviour that is potentially harmful, that is not entirely read-only, or that causes side effects while invoking a safe method"
  `<https://tools.ietf.org/html/rfc7231#section-4.2.1>`_;
* A method is *Idempotent* if the same operation repeated multiple times has the same side effect as using it one time.
  "repeating the request will have the same intended effect, even if the original request succeeded, though the response might differ"
  `<https://tools.ietf.org/html/rfc7231#section-4.2.2>`_.

You can find more information about HTTP Methods at `<https://restfulapi.net/http-methods/>`_

Uncommon HTTP Methods
^^^^^^^^^^^^^^^^^^^^^

* PATCH method allows to perform a differential update (i.e. an update which provides only the differences and not the whole new representation);
* PURGE method is not a standard but is widely used in services that require this action
  (e.g. `Varnish <https://varnish-cache.org/docs/3.0/tutorial/purging.html>`_, `Squid <https://wiki.squid-cache.org/SquidFaq/OperatingSquid#How_can_I_purge_an_object_from_my_cache.3F>`_).
  Hello World Service provides support for this method, but to support a wider range of clients, it also provides the Purge action via *DELETE* with the additional get parameter ``purge=true``.


Content-Type
------------

Any request must contain an indication of the interesting content type.

The client must specify the **Accept** HTTP Header for any operation returning a result. 

.. code-block:: rest

	Accept: application/json

For any operation sending content to the service, it is necessary to specify the **Content-Type** HTTP Header.

.. code-block:: rest

	Content-Type: application/json

The service accepts and returns only JSON objects.
 


Collections
-----------




Roles
-----


Clients
=======

Java Client
-----------

We provide the following Java Client out-of-the-box.

 .. TIP::
    If you're coding in Java, it is recommended that you use this Java Client.

**Maven Coordinates**

.. code:: xml

 <groupId>org.gcube.service</groupId>
 <artifactId>helloworld-client</artifactId>
 <version>[1.0.0-SNAPSHOT, 2.0.0-SNAPSHOT)</version>
 
**Methods Result**

The service exposes `its methods <../api-docs/index.html>`_ using a standard naming approach. Moreover, they accept (in the case of HTTP POST/PUT methods) JSON objects. 

 .. IMPORTANT::
   The result of all methods is always a JSON object as per below:

.. code:: javascript

	{
	    ....
	}

*Inputs are automatically validated before the request is served.*

      
**Usage examples**

- Example 1
      
.. code:: java

 import org.gcube....;


Postman Collection
------------------

Here you can download the `postman collection to interact with the service <../api-docs/postman/hello-world-service.postman_collection.json>`_.
You also need the environment to be selected to be able to generate the authorization token.
This is an example of `postman environment <../api-docs/postman/hello-world-service-for-devvre.postman_environment.json>`_ for devVRE (/gcube/devsec/devVRE).
The latter must be configured by inserting the value of your username and password used in the D4Science infrastructure.



Service Discovery on IS
=======================

The service can be discovered in the Facet-Based IS as EService with the following JSON query:

.. code:: json

	{
		"@class": "EService",
		"consistsOf": [
			{
				"@class": "IsIdentifiedBy",
				"target": {
					"@class": "SoftwareFacet",
					"group": "org.gcube.service",
					"name": "hello-world-service"
				}
			}
		]
	}
	
Service Maven Coordinates
=========================
	
The maven coordinates of the Hello World SG4 service are:

.. code:: xml

	<groupId>org.gcube.service</groupId>
	<artifactId>hello-world-service</artifactId>



