Dev and Pre Users used for moderation tests
========

To perform moderation tests in dev and preproduction infrastructure we use different users with the indicated roles.

.. table::

	+---------------+---------------+----------------------------------------+
	| User          | Username      | Role                                   |
	+===============+===============+========================================+
	| Mister Blonde | mister.blonde | Catalogue-Admin + Catalogue-Moderator  |
	+---------------+---------------+----------------------------------------+
	| Mister Blue   | mister.blue   | Catalogue-Admin                        |
	+---------------+---------------+----------------------------------------+
	| Mister Brown  | mister.brown  | Catalogue-Moderator                    |
	+---------------+---------------+----------------------------------------+
	| Mister Orange | mister.orange | Catalogue-Editor                       |
	+---------------+---------------+----------------------------------------+
	| Mister Pink   | mister.pink   | NO ROLE (means Catalogue-Member)       |
	+---------------+---------------+----------------------------------------+
	| Mister White  | mister.white  | Catalogue-Manager                      |
	+---------------+---------------+----------------------------------------+
	
