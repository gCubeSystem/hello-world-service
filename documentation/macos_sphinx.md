# WORKAROUND for sphinx maven plugin on M1 macs

the sphinx maven plugin is not working on M1 macs for java > 1.8

it tries to download the sphinx binary but it is not available for M1 macs

```
[ERROR] Failed to execute goal kr.motd.maven:sphinx-maven-plugin:2.10.0:
    generate (default) on project social-networking-library-ws: 
    Failed to run the report: unexpected response code '404':https://github.com/trustin/sphinx-binary/releases/download/v0.8.2/sphinx.osx-aarch_64 -> [Help 1]
```

java 1.8 uses the x86_64 binary which is perfectly working on M1 macs

so we can link the x86_64 binary to the aarch_64 binary, or manually download the x86 binary and provide it as it is the aarch binary

simlink: (only if the x86_64 binary is already downloaded in the local maven repository)

```sh
mkdir -p ~/.m2/repository/kr/motd/maven/sphinx-binary/https___github.com_trustin_sphinx-binary_releases_download_v0.8.2_sphinx.osx-aarch_64
cd ~/.m2/repository/kr/motd/maven/sphinx-binary
ln -s ../https___github.com_trustin_sphinx-binary_releases_download_v0.8.2_sphinx.osx-x86_64/sphinx.osx-x86_64 https___github.com_trustin_sphinx-binary_releases_download_v0.8.2_sphinx.osx-aarch_64/sphinx.osx-aarch_64
```

download (if the x86_64 binary is not available, or you want to dupkicate it)

```sh
mkdir -p ~/.m2/repository/kr/motd/maven/sphinx-binary/https___github.com_trustin_sphinx-binary_releases_download_v0.8.2_sphinx.osx-aarch_64
cd ~/.m2/repository/kr/motd/maven/sphinx-binary/https___github.com_trustin_sphinx-binary_releases_download_v0.8.2_sphinx.osx-aarch_64
wget https://github.com/trustin/sphinx-binary/releases/download/v0.8.2/sphinx.osx-x86_64 -O sphinx.osx-aarch_64
chmod +x sphinx.osx-aarch_64
```